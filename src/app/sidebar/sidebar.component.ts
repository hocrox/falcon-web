import {Component, OnInit, EventEmitter} from '@angular/core';
import { ISlimScrollOptions,SlimScrollEvent } from 'ngx-slimscroll';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {
  opts: ISlimScrollOptions;
  scrollEvents: EventEmitter<SlimScrollEvent>;

  constructor() { }

  ngOnInit() {
    this.scrollEvents = new EventEmitter<SlimScrollEvent>();
    this.opts = {
      position: 'right',
      barBackground: 'rgb(178, 178, 178)',
      barWidth: '4',
      gridWidth: '2',
      barBorderRadius: '4',
      gridBackground:'rgba(0, 0, 0, 0.09)'
    }
  }
  hu(){
    alert('fffffffff');
  }

}
