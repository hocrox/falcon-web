import{NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {LoginComponent} from './login/login.component';
import {HomepageComponent} from './homepage/homepage.component';
import {WebSocketComponent} from './web-socket/web-socket.component';
import {ChannelComponent} from './channel/channel.component';
import {GroupComponent} from "./group/group.component";




const routes: Routes = [
  {path: '', pathMatch: 'full', redirectTo: 'login'},
  {path: '', component: LoginComponent},
  {path: 'homepage', component: HomepageComponent},
  {path: 'socket', component: WebSocketComponent},
  {path: 'channel', component: ChannelComponent},
  {path: 'group', component: GroupComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {

}
export const routableComponents = [

];
