import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from "@angular/http";
import { AppRoutingModule,routableComponents} from './app.routing';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { NgSlimScrollModule } from 'ngx-slimscroll';

// components
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { HomepageComponent } from './homepage/homepage.component';
import { HeaderComponent } from './header/header.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { ChatHeaderComponent } from './chat-header/chat-header.component';
import { ChannelComponent } from './channel/channel.component';
import { WebSocketComponent } from './web-socket/web-socket.component';
import { GroupComponent } from './group/group.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HomepageComponent,
    HeaderComponent,
    SidebarComponent,
    ChatHeaderComponent,
    ChannelComponent,
    WebSocketComponent,
    GroupComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    AppRoutingModule,
    NgbModule.forRoot(),
    NgSlimScrollModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
