import {Headers} from '@angular/http';

export class config {


  public static getEnvironmentVariable(value) {
    let serverip = 'http://192.168.0.8:8092/';
    // let serverip = 'http://45.79.165.204:8092/';
    return serverip;

  }


  public static getHeader() {
    let jwt = localStorage.getItem('crusher_token');
    let authHeader = new Headers({'Content-Type': 'application/json'});
    if (jwt) {
      authHeader.append('Authorization', 'Bearer ' + jwt);
    }
    return authHeader;

  }

}
