import {Component, OnInit, EventEmitter} from '@angular/core';
import { ISlimScrollOptions,SlimScrollEvent } from 'ngx-slimscroll';


@Component({
  selector: 'app-homepage',
  templateUrl: './homepage.component.html',
  styleUrls: ['./homepage.component.scss']
})
export class HomepageComponent implements OnInit {
  opts: ISlimScrollOptions;
  scrollEvents: EventEmitter<SlimScrollEvent>;
  constructor() { }

  ngOnInit() {
    this.scrollEvents = new EventEmitter<SlimScrollEvent>();
    this.opts = {
      position: 'right',
      barBackground: 'rgb(178, 178, 178)',
      barWidth: '4',
      gridWidth: '2',
      barBorderRadius: '4',
      gridBackground:'rgba(0, 0, 0, 0.09)'
    }
  }

}
