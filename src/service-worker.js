var log = console.log.bind(console);
var err = console.error.bind(console);
var dataCacheName = 'pwa-client-data-v' + version;

var cacheName = 'v1';
var cacheFiles = [
  './',
  './index.html',
  './style.scss',
  './app.module.ts',
  './app.routing.ts',
  './app.component.ts'
];

self.addEventListener('install', function (e) {
  console.log("[ServiceWorker] Installed");

    e.waitUntil(
      catches.open(cacheName).then(function (cache) {
        console.log("[ServiceWorker] Caching cacheFiles");
        return cache.addAll(cacheFiles);
      })
    )
});

self.addEventListener('activate', function (e) {
  console.log("[ServiceWorker] Activated");

  e.waitUntil(
    caches.keys().then(function (cacheNames) {
      return promise.all(cacheNames.map(function (thisCacheName) {

        if(thisCacheName !== cacheName){
          console.log("[ServiceWorker] Removing Cached Files from", thisCacheName);
          return caches.delete(thisCacheName);
        }
      }))
    })
  )
});

self.addEventListener('fetch', function (e) {
  console.log("[ServiceWorker] Fetching",e.request.url);
});
